/**
 * File manager
 * /lib/file-manager.js
 */

'use strict';

var
    fs = require('fs'),
    url = require('url'),
    path = require('path'),
    humanize = require('humanize'),
    uuid = require('node-uuid');

function FileManager() {}

/**
 * Checks whether directory exists. Creates if doesn't
 * @param dir {String} - directory
 * @private
 */
// todo implement like recursive checker check(dir, number), where number is a number of recursive steps
FileManager.prototype._checkPath = function(dir) {
    var result = path.join(dir, '..', '..');
    var parent = path.join(dir, '..');

    var paths = [
        result,
        parent,
        dir
    ];

    paths.forEach(function(dir) {
        try {
            // throws error if directory doesn't exist
            fs.lstatSync(dir);
        } catch(e) {
            // dir doesn't exist
            // create dir
            try {
                fs.mkdirSync(dir);
            } catch(e) {
                // race
                if(e.code === 'EEXIST') {
                    // ok
                    return;
                }
            }
        }
    });
};

/**
 * Initializes file-manager
 * @param dir - working directory
 */
FileManager.prototype.init = function(dir) {
    this._dir = dir;
};

/**
 * Returns configuration file
 * @returns {{}}
 */
FileManager.prototype.getConfig = function() {
    var config = {};

    try {
        var bytes = fs.readFileSync(path.join(this._dir, 'config.json'), 'utf8');

        config = JSON.parse(bytes);

        return config;
    } catch (e) {
        console.log('%d | %s | Error has occurred while reading config.js: ' + e,
            process.pid,
            humanize.date('d.m.Y H:i:s'));
        return config;
    }
};

/**
 * Writes file
 * @param page {Object} - page
 * @param body {String} - body
 * @param cb {Function} - callback
 */
FileManager.prototype.write = function(page, body, cb) {
    var options = url.parse(page.base);
    var dir = path.join(this._dir, '/result', options.host, page.depth + '');
    var file = path.join(this._dir, '/result', options.host, page.depth + '', uuid.v1() + '.html');

    this._checkPath(dir);

    fs.writeFile(file, body, function(e) {
        if(e) {
            console.log('%d | %s | Error while writing the page %s: %s',
                process.pid,
                humanize.date('d.m.Y H:i:s'),
                page.url,
                e.toString() + '\n' + e.stack.toString());

            return;
        }

        console.log('%d | %s | Page %s was written to file. Depth: %s',
            process.pid,
            humanize.date('d.m.Y H:i:s'),
            page.url,
            page.depth);

        if(cb) {
            cb();
        }
    });
};

module.exports = new FileManager();