/**
 * Process manager
 * /lib/process-manager
 */

'use strict';

var
    os = require('os'),
    cluster = require('cluster'),
    collection = require('./collection');

function ProcessManager(){}

/**
 * Sends next page to parse
 * @param worker {Object} - worker
 * @private
 */
ProcessManager.prototype._next = function(worker) {
    var next = collection.next();
    if(next) {
        worker.send({
            page: next
        });
    }
};

ProcessManager.prototype._ok = function(worker) {
    worker.send({
        cmd: 'ok'
    });
};

/**
 * Initializes cluster processes
 * @param dir {String} - working dir
 */
ProcessManager.prototype.init = function(dir) {
    cluster.setupMaster({
        exec: 'app.js'
    });

    for(var i = 0; i < os.cpus().length; i++) {
        var worker = cluster.fork({
            dir: dir
        });

        var that = this;

        worker.once('online', function(address) {
            that._next(this);
        });

        worker.on('message', function(data) {
            if(data.cmd === 'next') {
                that._next(this);
            }

            if(data.cmd === 'add') {
                if(data.pages) {
                    var depth = collection.depth();
                    var pages = data.pages.filter(function(page) {
                        return page.depth <= depth;
                    });

                    pages.forEach(function(page) {
                        collection.add(page);
                    });
                    that._ok(this);
                }
            }
        })
    }
};

module.exports = new ProcessManager();