/**
 * Collection of documents to parse
 * /lib/collection.js
 */

'use strict';

var
    url = require('url');

function Collection() {}

/**
 * Checks whether url has been met already
 * @param url {String} - url
 * @returns {boolean}
 * @private
 */
Collection.prototype._exist = function(url) {
    var exists = false;

    this._queue.every(function(page) {
        if(page.url === url) {
            exists = true;
            return false;
        } else {
            return true;
        }
    });

    return exists;
};

/**
 * Checks whether url has been past already
 * @param url {String} - url
 * @returns {boolean}
 * @private
 */
Collection.prototype._met = function(url) {
    var met = false;

    this._passed.every(function(page) {
        if(page.url === url) {
            met = true;
            return false;
        } else {
            return true;
        }
    });

    return met;
};

/**
 * Initializes collection with empty values
 * @param base {Array} - base sites
 * @param args {Object} - inline args
 */
Collection.prototype.init = function(base, args) {
    this._base = base;
    this._depth = args.depth;
    this._method = args.method;
    this._queue = [];
    this._passed = [];

    for(var i = 0; i < base.length; i++) {
        var options = url.parse(base[i]);

        this._queue.push({
            url: base[i],
            depth: 0,
            base: base[i]
        });
    }

};

/**
 * Returns base sites
 * @returns {Array} base queue
 */
Collection.prototype.getBase = function() {
    return this._base;
};

/**
 * Returns depth
 * @returns {number|Function|depth|*}
 */
Collection.prototype.depth = function() {
    return this._depth;
};

/**
 * Returns next page to parse
 * @returns {*|T}
 */
Collection.prototype.next = function() {
    var next = this._method === 'stack' ? this._queue.pop() : this._queue.shift();
    if(next) {
        this._passed.push(next);

        return next;
    }
};

/**
 * Adds new element to the collection
 * @param page {Object} - page
 */
Collection.prototype.add = function(page) {
    var exists = this._exist(page.url);
    var passed = this._met(page.url);

    if(!exists && !passed) {
        this._queue.push(page);
    }
};

module.exports = new Collection();