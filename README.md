# This is http / https document - downloader that is built for you. #

*   Build your site list you want to download;
*   Specify depth (amount of steps from first source followed by external links) you want to get in;
*   Specify workaround (download method) you want to use.


## How do I get set up? ##

*   Install **Node.js** version 0.12.2 or newer;
*   Go to project folder;
*   Install all dependencies: from console **npm install**;
*   See **config.json.example** as an example in project folder to build your own configuration file .json;
*   Launch program:

```console
node master.js --config "filepath" --depth number --method "stack or queue"
```

(type `-h` to see command help)

## FAQ ##

Why does the program never stop?

> It won't stop automatically. Follow console until all processes stop downloading.

Where can I see parsed results?

> Result folder will be created in the same folder as configuration file