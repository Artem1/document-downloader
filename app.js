/**
 * Child process
 * app.js
 */

'use strict';

var
    http = require('http'),
    https = require('https'),
    url = require('url'),
    zlib = require('zlib'),
    free = true,
    timeout = 1000,
    humanize = require('humanize'),
    fileManager = require('./lib/file-manager.js'),
    htmlparser2 = require('htmlparser2'),
    references = [],
    pages = [];

/*
Initialize parser
 */
var parser = new htmlparser2.Parser({
    onopentag: function(tag, attr){
        if(tag === 'a') {
            var ref = attr.href;

            if(!ref) {
                return;
            }

            if(ref.charAt(0) === '#') {
                // DOM element reference
                return;
            }

            references.push(ref);
        }
    }
});

/**
 * Downloads page
 * @param page {String} - page url
 * cb {Function} - callback
 */
var request = function(page, cb) {
    var options = url.parse(page);
    var protocol = options.protocol === 'http:' ? http : https;
    var port = options.protocol === 'http:' ? 80 : 443;
    var req = protocol.get({
        host: options.host,
        path: options.path,
        port: port
    }, function(res) {
        console.log('%d | %s | HTTP request %s. Server responded with status %s',
            process.pid,
            humanize.date('d.m.Y H:i:s'),
            page,
            res.statusCode);

        console.log('%d | %s | HTTP request %s. Headers %s',
            process.pid,
            humanize.date('d.m.Y H:i:s'),
            page,
            JSON.stringify(res.headers));

        var bodyChunks = [];

        // be aware of content encoding g-zip
        if(res.headers['content-encoding'] === 'gzip' ) {
            var gzip = zlib.createGunzip();
            res.pipe(gzip);

            gzip.on('data', function(chunk) {
                bodyChunks.push(chunk.toString('utf-8'));
            });

            gzip.on('end', function () {
                // set encoding
                var body = bodyChunks.join('');

                if (cb) {
                    cb(body);
                }
            });
        } else {
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                bodyChunks.push(chunk);
            });
            res.on('end', function () {
                // set encoding
                var body = bodyChunks.join('');

                if (cb) {
                    cb(body);
                }
            });
        }
    });

    req.on('error', function(e) {
        console.log('%d | %s | HTTP request %s. Error %s',
            process.pid,
            humanize.date('d.m.Y H:i:s'),
            page,
            e.toString() + '\n' + e.stack.toString());
    });
};

/**
 * Checks whether link is external link
 * @param base {String} - base page
 * @param ref {String} - link to check
 * @returns {boolean}
 */
var isExternal = function(base, ref) {
    var isExternal = url.parse(base).host !== url.parse(ref).host;

    return isExternal;
};

/*
 Initialize file manager for further work
 */
fileManager.init(process.env.dir);

/*
Error handling
 */
process.on('uncaughtException', function(e) {
    console.log('%d | %s | Uncaught exception: %s',
        process.pid,
        humanize.date('d.m.Y H:i:s'),
        e.toString() + '\n' + e.stack.toString());
    process.exit();
});

process.on('message', function(data) {
    if(data.page) {
        // trigger
        free = false;

        setTimeout(request(data.page.url, function(body) {
            fileManager.write(data.page, body, function() {
                // parse references
                parser.write(body);
                parser.end();

                references.forEach(function(ref) {
                    var external = isExternal(data.page.url, url.resolve(data.page.url, ref));
                    var depth = external ? data.page.depth + 1 : data.page.depth;

                    pages.push({
                        url: url.resolve(data.page.url, ref),
                        depth: depth,
                        base: data.page.base
                    });
                });

                process.send({
                    cmd: 'add',
                    pages: pages
                });
            });
        }), timeout);
    }

    if(data.cmd === 'ok') {
        pages = [];
        // trigger
        free = true;
    }
});

console.log('%d | %s | Worker %s has started',
    process.pid,
    humanize.date('d.m.Y H:i:s'),
    process.pid);

/*
Ask for next page
 */
setInterval(function() {
    if(free) {
        process.send({
            cmd: 'next'
        });
    }
}, timeout);
