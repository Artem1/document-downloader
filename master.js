/**
 * Master process
 * master.js
 */

'use strict';

var
    argumentParser = require('argparse').ArgumentParser,
    version = require('./package.json').version,
    fileManager = require('./lib/file-manager.js'),
    collection = require('./lib/collection.js'),
    processManager = require('./lib/process-manager.js'),
    humanize = require('humanize');

/*
 Error handling
 */
process.on('uncaughtException', function(e) {
    console.log('%d | %s | Uncaught exception %s',
        process.pid,
        humanize.date('d.m.Y H:i:s'),
        e.toString() + '\n' + e.stack.toString());
    process.exit();
});

/*
Parse arguments
 */
var parser = new argumentParser({
    version: version,
    addHelp: true,
    description: 'Argument parser'
});

parser.addArgument(['--dir'], {
        help: 'Working directory'
    }
);
parser.addArgument(['--depth'], {
        help: 'Depth',
        defaultValue: 0
    }
);
parser.addArgument(['--method'], {
        help: 'Workaround. Stack or queue',
        defaultValue: 'stack'
    }
);

var args = parser.parseArgs();

/*
Argument parsing errors
 */
if(!args || !args.dir) {
    console.log('%d | %s | Error. Working directory is not specified',
        process.pid,
        humanize.date('d.m.Y H:i:s'));
    process.exit(43);
}

if(args.method.toLowerCase() !== 'stack' && args.method.toLowerCase() !== 'queue') {
    console.log('%d | %s | Error. Not supported workaround',
        process.pid,
        humanize.date('d.m.Y H:i:s'));
    process.exit(43);
}

/*
Initialize file manager for further work
 */
fileManager.init(args.dir);

/*
Initialize collection
 */
var base = fileManager.getConfig().sites;
collection.init(base, args);

/*
Initialize cluster
 */
processManager.init(args.dir);

/*
All OK
 */
console.log('All systems functional');
